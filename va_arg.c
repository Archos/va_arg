#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

char* strnext(char *str, const char *delimiters)
{
    str += strcspn(str, delimiters);
    str += strspn(str, delimiters);
    return str;
}

void print(char *str, const char *delimiters, int casc, ...)
{
    va_list arg, cur;
    va_start(arg, casc);

    char key;
    void (*action)(char *, void *);
    void *data;

    int i;
    while (*str) {
	va_copy(cur, arg);

	for (i = 0; i < casc / 3; ++i) {
	    key = va_arg(cur, int);
	    action = va_arg(cur, void (*)(char *, void *));
	    data = va_arg(cur, void *);

	    if (key == *str) action(str, data);
	}

	va_end(cur);
	str = strnext(str, " ");
    }

    va_end(arg);
}

void add(char *str, void *data)
{
    float *num = (float*)(data);
    str += strcspn(str, "0123456789+-.");
    *num += atof(str);
}

void sub(char *str, void *data)
{
    float *num = (float*)(data);
    str += strcspn(str, "0123456789+-.");
    *num -= atof(str);
}

void mul(char *str, void *data)
{
    float *num = (float*)(data);
    str += strcspn(str, "0123456789+-.");
    *num *= atof(str);
}

void dvd(char *str, void *data)
{
    float *num = (float*)(data);
    str += strcspn(str, "0123456789+-.");
    *num /= atof(str);
}

int main(int argc, char *argv[])
{
    float num = 0;

    if (argc > 1) 
	print(argv[1], " ", 12, 
	    'A', add, &num, 
	    'S', sub, &num, 
	    'M', mul, &num, 
	    'D', dvd, &num
	);

    printf("Number: %g\n", num);
    return 0;
}
